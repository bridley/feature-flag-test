FROM golang:1.20
COPY . /
WORKDIR /
RUN go build -o fft

FROM debian:bullseye-slim  
COPY --from=0 ./fft /usr/local/bin/fft
CMD ["/usr/local/bin/fft"]