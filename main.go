package main

import (
	"embed"
	"io/fs"
	"net/http"
//	"os"

//	"github.com/Unleash/unleash-client-go/v3"
)

// content holds our static web server content.
//
//go:embed html/*
var content embed.FS

// func init() {
// 	// apiToken := os.Getenv("UNLEASH_API_TOKEN")
// 	// unleash.Initialize(
// 	// 	unleash.WithListener(&unleash.DebugListener{}),
// 	// 	unleash.WithAppName("my-application"),
// 	// 	unleash.WithUrl("http://unleash.herokuapp.com/api/"),
// 	// 	unleash.WithCustomHeaders(http.Header{"Authorization": {apiToken}}),
// 	// )
// }

func main() {
	htmlFs, err := fs.Sub(content, "html")
	if err != nil {
		panic(err)
	}

	http.Handle("/", http.FileServer(http.FS(htmlFs)))
	http.ListenAndServe("0.0.0.0:8080", nil)
}
